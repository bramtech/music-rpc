# music-rpc

> Repo moved to my Github! https://github.com/bramtechs/music-rpc

## One Discord script to rule them all!

music-rpc is a simple python script that displays rich-presence of your music player on your discord profile.
Currently supported music players are:
- Audacious
- Rhythmbox
- cmus
- sayonara

This script needs **playerctl** and the pip3 library **discord_rpc** in order to function. Make sure they're installed!

Ubuntu/Debian
```bash
sudo apt install playerctl
pip3 install discord-rpc.py
```

## Screenshots
### Standard
soon™

### Palms
![examples](screenshots/examples.png "examples")
